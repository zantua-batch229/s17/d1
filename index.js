// console.log("Hello")

// [SECTION] - Functions
// Functions in JS are lines/blocks of codes that will tell our devices or apps to perform a particular task.
// Functions are mostly created complicated tasks to run several lines of code succession.
// they are also used to prevent reopeating line of codes

// Function Declaration -> "function"


/*Syntax:
function functionName(){
	
	//code block
	let num = 10;
	console.log(num); no error
}

console.log(num); error -because its outside the {}

*/

function printName(){
	console.log("My name is John");

}
// calling or invoke/invocation
printName();
printName();

function printGrade(g1, g2){
	let ave = (g1+g2)/2
	console.log(ave)
}
printGrade(90, 80);

// [SECTION] Function Invocation


// [SECTION] - Function Declaration vs expression

// This is a function declaration
function declaredFunction(){
	console.log("Hello World from declaredFunction()");
}

declaredFunction();

// This is a function expression
// A function cal alse be stored in a variable. This is called a function expression.

// Anonymous function - a function without a name

// variableFunction();
/*
	error function expressions being stored in a let or const cannot be hoisted

*/
// Anonymous function
// Function Expression Example

let variableFunction = function(){
	console.log("Hello Again!")
}

variableFunction();

let functionExpression = function funcName(){
	console.log("Hello expression!")
}

functionExpression(); //this is the right invocation in fucntion expressione
//funcName(); //this will return an error

// you can reassign declared functions and functions expression to a new anonymous finction

declaredFunction = function(){
	console.log("updated declaredFunction()")
}
declaredFunction();

functionExpression = function(){
	console.log("updated functionExpression()");
}
functionExpression();

// However, we cannot re-assign a function expression initialized with const

// [section] - function scoping
/*
1. local/block scope
2. global scope
3. function scope

*/

{

	// this is a local variable and its value is only accessible inside the curly braces
	let localVariable = "Armando Perez";
	console.log(localVariable);
}

// this is a global variable and its value is accessible inside anywhere in the codebase
let globalVariable = "Mr. Worldwide";
console.log(globalVariable);
// console.log(localVariable); will return an error

function showNames(){
	// Function scoped variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);

}

showNames();

// Nested function

function myNewFunction(){

	let name = "Jane2";

	function nestedFunction(){
		let nestedName = "John";
		console.log(name);
	}
	nestedFunction();
}

myNewFunction();
//nestedFunction(); error - enclosed in curly braces


// Function and Global Scope Variables

let globalName = "Alexandro";

function myNewFunction2(){

	let nameInside = "Renz";
	console.log(globalName);
}

myNewFunction2();

// [SECTION] - Using Alert
//alert() allows us to show small window at the top of our browser.

//alert("Hello World");

function showSampleAlert(){
	alert("Hello user");
}
//showSampleAlert();
// console.log("I will only log in the console when the alert is dismissed");
// Notes on the use of the alert()
	// show only alert for short dialog message.
	// do not overuse alert() because program has to wait to be dismissed before continuing

// [SECTION] USing prompt()
// allows us to show small window and gather user input.
//let samplePrompt =  prompt("Enter you name:");

//console.log("Hello " + samplePrompt);
//console.log(typeof samplePrompt); //automatically converts into string

// let sampleNullPrompt = prompt("Do not enter anything");
// console.log(sampleNullPrompt); //returns empty string

/*function printWelcomeMessage(){
	let firstName = prompt("Enter you first name:");
	let lastName = prompt("Enter you last name:");

	console.log("Hello " + firstName + " " + lastName + "!");
	console.log("welcome to my page!");
}*/
//printWelcomeMessage();

// [SECTION] - Function naming convention
// Function names should be definitive of the tast it will perform. usually contains verb

function getCourse(){

	let courses = ["Science 101", "Math 101", "English 101"];
	console.log(courses);
}
getCourse();

//avoid generic names to avoid confusion within your code